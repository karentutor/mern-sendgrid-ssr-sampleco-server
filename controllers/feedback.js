exports.emailFeedback = (req, res) => {
  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(
    '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'
  );
  // console.log(process.env.SENDGRID_API_KEY);
  // // console.log(req.body);
  const { name, email, message, phone, uploadedFiles } = req.body;
  const emailData = {
    to: email,
    from: process.env.EMAIL_FROM,
    subject: 'Feedback form',
    text: 'test',
    html: 'test',
    html: `
            <h1>Customer Feedback Form</h1>
            <hr />
            <h2>Sender name: ${name}</h2>
            <h2>Sender email: ${email}</h2>
            <h2>Sender message: ${message}</h2>
            <br />
            ${uploadedFiles.map((f) => {
              return `<img src="${f.secure_url}" alt="${f.original_filename}" style="width:50%;overflow:hidden;padding:50px;" />`;
            })}
            <hr />
            <p>https://feedbackonline.com</p>
        `,
  };

  sgMail
    .send(emailData)
    .then((sent) => {
      console.log(sent);
      return res.json({
        success: true,
      });
    })
    .catch((err) => {
      console.log(err);
      return res.json({
        success: false,
      });
    });
};
